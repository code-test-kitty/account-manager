package com.acmebank.accountmanager.controller;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.acmebank.accountmanager.dto.AccountDto;
import com.acmebank.accountmanager.dto.TransactionDto;
import com.acmebank.accountmanager.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountManagerController.class)
public class AccountManagerControllerTest {

  @Autowired ObjectMapper objectMapper;

  @Autowired MockMvc mvc;

  @MockBean AccountService accountService;

  @Test
  public void getTest() throws Exception {

    AccountDto accountDto = new AccountDto("12345678", BigDecimal.valueOf(1000000));

    given(accountService.get("12345678")).willReturn(accountDto);

    mvc.perform(get("/account/12345678").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void transferAmountTest() throws Exception {

    TransactionDto invalidDto = new TransactionDto(null, "", "", BigDecimal.valueOf(1000000));

    given(accountService.transferAmount(invalidDto)).willReturn(invalidDto);

    mvc.perform(
            post("/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invalidDto)))
        .andExpect(status().isBadRequest());

    TransactionDto validDto =
        new TransactionDto(null, "12345678", "88888888", BigDecimal.valueOf(1000000));

    given(accountService.transferAmount(validDto)).willReturn(validDto);

    mvc.perform(
            post("/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(validDto)))
        .andExpect(status().isOk());
  }
}
