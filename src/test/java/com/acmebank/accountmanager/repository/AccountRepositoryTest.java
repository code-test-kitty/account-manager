package com.acmebank.accountmanager.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.acmebank.accountmanager.entity.Account;
import com.acmebank.accountmanager.respository.AccountRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@Log4j2
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class AccountRepositoryTest {

  @Autowired TestEntityManager entityManager;
  @Autowired AccountRepository accountRepository;

  Account account;

  @BeforeEach
  public void setUp() {
    account = new Account(null, "123456", BigDecimal.valueOf(10000), 0);
    account.setCreatedBy("test");
    account.setUpdatedBy("test");
    account.setCreatedDate(LocalDateTime.now());
    account.setUpdatedDate(LocalDateTime.now());
    entityManager.persist(account);
    entityManager.flush();
  }

  @Test
  public void findByAccountNumberTest() {

    Account found = accountRepository.findByAccountNumber(account.getAccountNumber()).orElseThrow();

    assertThat(found.getAccountNumber()).isEqualTo(account.getAccountNumber());
  }

  @Test
  public void findByAccountNumberAndAmountGreaterThanEqualTest() {

    Account found =
        accountRepository
            .findByAccountNumberAndAmountGreaterThanEqual(
                account.getAccountNumber(), account.getAmount())
            .orElseThrow();

    assertThat(found.getAccountNumber()).isEqualTo(account.getAccountNumber());

    accountRepository
        .findByAccountNumberAndAmountGreaterThanEqual(
            account.getAccountNumber(), BigDecimal.valueOf(10000000))
        .orElse(null);

    assertThat(
            accountRepository
                .findByAccountNumberAndAmountGreaterThanEqual(
                    account.getAccountNumber(), BigDecimal.valueOf(10000000))
                .orElse(null))
        .isNull();
  }
}
