package com.acmebank.accountmanager.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.acmebank.accountmanager.dto.AccountDto;
import com.acmebank.accountmanager.dto.TransactionDto;
import com.acmebank.accountmanager.entity.Account;
import com.acmebank.accountmanager.respository.AccountRepository;
import com.acmebank.accountmanager.respository.TransactionRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class AccountServiceTest {

  @MockBean AccountRepository accountRepository;
  @MockBean TransactionRepository transactionRepository;

  @Autowired AccountService accountService;

  Account toAccount;
  Account fromAccount;
  TransactionDto transactionDto;

  @BeforeEach
  public void setUp() {
    accountRepository = Mockito.mock(AccountRepository.class);
    transactionRepository = Mockito.mock(TransactionRepository.class);
    accountService = new AccountService(accountRepository, transactionRepository);

    toAccount = new Account(null, "12345678", BigDecimal.valueOf(1000000), 0);
    toAccount.setCreatedBy("test");
    toAccount.setUpdatedBy("test");
    toAccount.setCreatedDate(LocalDateTime.now());
    toAccount.setUpdatedDate(LocalDateTime.now());

    fromAccount = new Account(null, "88888888", BigDecimal.valueOf(1000000), 0);
    fromAccount.setCreatedBy("test");
    fromAccount.setUpdatedBy("test");
    fromAccount.setCreatedDate(LocalDateTime.now());
    fromAccount.setUpdatedDate(LocalDateTime.now());

    Mockito.when(accountRepository.findByAccountNumber(toAccount.getAccountNumber()))
        .thenReturn(Optional.of(toAccount));
    Mockito.when(accountRepository.findByAccountNumber(fromAccount.getAccountNumber()))
        .thenReturn(Optional.of(fromAccount));
    Mockito.when(
            accountRepository.findByAccountNumberAndAmountGreaterThanEqual(
                fromAccount.getAccountNumber(), BigDecimal.valueOf(500)))
        .thenReturn(Optional.of(fromAccount));
  }

  @Test
  public void getAccountTest() {
    AccountDto accountDto = accountService.get("12345678");

    assertThat(accountDto.getAccountNumber()).isEqualTo("12345678");
    assertThat(accountDto.getAmount()).isEqualTo(BigDecimal.valueOf(1000000));
  }

  @Test
  public void transferAmountTest() {

    transactionDto =
        new TransactionDto(
            null,
            fromAccount.getAccountNumber(),
            toAccount.getAccountNumber(),
            BigDecimal.valueOf(500));
    accountService.transferAmount(transactionDto);
  }

  @Test
  public void convertToDtoAmtTest() {
    assertThat(accountService.convertToDtoAmt(BigDecimal.valueOf(10), 0))
        .isEqualTo(BigDecimal.valueOf(10));
    assertThat(accountService.convertToDtoAmt(BigDecimal.valueOf(1001), 2))
        .isEqualTo(BigDecimal.valueOf(10.01));
    assertThat(accountService.convertToDtoAmt(BigDecimal.valueOf(12300112), 4))
        .isEqualTo(BigDecimal.valueOf(1230.0112));
    assertThat(accountService.convertToDtoAmt(BigDecimal.valueOf(1230011), 3))
        .isEqualTo(BigDecimal.valueOf(1230.011));
  }

  @Test
  public void getDecimalPlaceTest() {
    assertThat(accountService.getDecimalPlace(BigDecimal.valueOf(10.032))).isEqualTo(3);
    assertThat(accountService.getDecimalPlace(BigDecimal.valueOf(10.02))).isEqualTo(2);
    assertThat(accountService.getDecimalPlace(BigDecimal.valueOf(10.2))).isEqualTo(1);
    assertThat(accountService.getDecimalPlace(BigDecimal.valueOf(10.0))).isEqualTo(1);
    assertThat(accountService.getDecimalPlace(BigDecimal.valueOf(10))).isEqualTo(0);
    assertThat(accountService.getDecimalPlace(BigDecimal.valueOf(1230.0110))).isEqualTo(3);
  }

  @Test
  public void convertToDbAmtTest() {

    assertThat(accountService.convertToDbAmt(BigDecimal.valueOf(10)))
        .isEqualTo(BigDecimal.valueOf(10));
    assertThat(accountService.convertToDbAmt(BigDecimal.valueOf(10.01)))
        .isEqualTo(BigDecimal.valueOf(1001));
    assertThat(accountService.convertToDbAmt(BigDecimal.valueOf(1230.0112)))
        .isEqualTo(BigDecimal.valueOf(12300112));
    assertThat(accountService.convertToDbAmt(BigDecimal.valueOf(1230.0110)))
        .isEqualTo(BigDecimal.valueOf(1230011));
  }
}
