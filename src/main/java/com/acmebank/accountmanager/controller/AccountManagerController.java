package com.acmebank.accountmanager.controller;

import com.acmebank.accountmanager.dto.AccountDto;
import com.acmebank.accountmanager.dto.ResponsePayload;
import com.acmebank.accountmanager.dto.RestResponseEntity;
import com.acmebank.accountmanager.dto.TransactionDto;
import com.acmebank.accountmanager.service.AccountService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountManagerController {

  @Autowired AccountService accountService;

  @GetMapping("/account/{accountNumber}")
  public ResponseEntity<ResponsePayload<AccountDto>> get(@PathVariable String accountNumber) {
    return RestResponseEntity.ok(accountService.get(accountNumber));
  }

  @PostMapping("/transfer")
  public ResponseEntity<ResponsePayload<TransactionDto>> transfer(
      @RequestBody @Valid TransactionDto transactionDto) {
    return RestResponseEntity.ok(accountService.transferAmount(transactionDto));
  }
}
