package com.acmebank.accountmanager.service;

import com.acmebank.accountmanager.dto.AccountDto;
import com.acmebank.accountmanager.dto.TransactionDto;
import com.acmebank.accountmanager.entity.Account;
import com.acmebank.accountmanager.entity.Transaction;
import com.acmebank.accountmanager.respository.AccountRepository;
import com.acmebank.accountmanager.respository.TransactionRepository;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Service
public class AccountService {

  @Autowired AccountRepository accountRepository;
  @Autowired TransactionRepository transactionRepository;

  @Transactional(readOnly = true)
  public AccountDto get(String accountNumber) {
    Account account = accountRepository.findByAccountNumber(accountNumber).orElseThrow();
    AccountDto dto = new AccountDto();
    dto.setAccountNumber(account.getAccountNumber());
    dto.setAmount(convertToDtoAmt(account.getAmount(), account.getDecimalPlace()));
    return dto;
  }

  @Transactional
  public TransactionDto transferAmount(TransactionDto transactionDto) {
    Account fromAccount =
        accountRepository
            .findByAccountNumberAndAmountGreaterThanEqual(
                transactionDto.getFormAccountName(), convertToDbAmt(transactionDto.getAmount()))
            .orElseThrow();
    Account toAccount =
        accountRepository.findByAccountNumber(transactionDto.getToAccountName()).orElseThrow();

    String transactionId = UUID.randomUUID().toString();

    saveTransaction(fromAccount, toAccount, transactionDto.getAmount(), transactionId);

    saveAccount(fromAccount, toAccount, transactionDto.getAmount());

    transactionDto.setTransactionId(transactionId);

    return transactionDto;
  }

  @Transactional
  public void saveTransaction(
      Account fromAccount, Account toAccount, BigDecimal dtoAmount, String transactionId) {
    int decimalPlace = getDecimalPlace(dtoAmount);
    BigDecimal dbAmount = convertToDbAmt(dtoAmount);

    Transaction credit =
        Transaction.builder()
            .accountId(toAccount.getId())
            .transactionId(transactionId)
            .amount(dbAmount)
            .decimalPlace(decimalPlace)
            .credit(true)
            .build();

    Transaction debit =
        Transaction.builder()
            .accountId(fromAccount.getId())
            .transactionId(transactionId)
            .amount(dbAmount)
            .decimalPlace(decimalPlace)
            .credit(false)
            .build();

    transactionRepository.save(credit);
    transactionRepository.save(debit);
  }

  @Transactional
  public void saveAccount(Account fromAccount, Account toAccount, BigDecimal dtoAmount) {

    BigDecimal fromAcAmt =
        convertToDtoAmt(fromAccount.getAmount(), fromAccount.getDecimalPlace()).subtract(dtoAmount);

    fromAccount.setAmount(convertToDbAmt(fromAcAmt));
    fromAccount.setDecimalPlace(getDecimalPlace(fromAcAmt));

    BigDecimal toAcAmt =
        convertToDtoAmt(toAccount.getAmount(), toAccount.getDecimalPlace()).add(dtoAmount);

    toAccount.setAmount(convertToDbAmt(toAcAmt));
    toAccount.setDecimalPlace(getDecimalPlace(toAcAmt));

    accountRepository.save(fromAccount);
    accountRepository.save(toAccount);
  }

  public BigDecimal convertToDtoAmt(BigDecimal amount, Integer decimalPlace) {
    return amount.scaleByPowerOfTen(decimalPlace * -1);
  }

  public int getDecimalPlace(BigDecimal amount) {
    return amount.scale();
  }

  public BigDecimal convertToDbAmt(BigDecimal amount) {
    return amount.scaleByPowerOfTen(getDecimalPlace(amount));
  }
}
