package com.acmebank.accountmanager.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ResponsePayload<T> {
  private long timestamp;
  private T data;
  private int code;
  private boolean success;
  private String message;
}
