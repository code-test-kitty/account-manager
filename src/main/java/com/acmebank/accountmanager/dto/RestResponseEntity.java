package com.acmebank.accountmanager.dto;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Log4j2
public class RestResponseEntity {

  public static <T> ResponseEntity<ResponsePayload<T>> ok(T data) {
    return ok(data, null);
  }

  public static <T> ResponseEntity<ResponsePayload<T>> ok(T data, String message) {
    return ResponseEntity.ok(
        ResponsePayload.<T>builder()
            .data(data)
            .timestamp(getTimestamp())
            .code(0)
            .success(true)
            .message(message)
            .build());
  }

  public static <T> ResponseEntity<ResponsePayload<T>> error(
      int code, String message, HttpStatus status) {
    return ResponseEntity.status(status)
        .body(
            ResponsePayload.<T>builder()
                .timestamp(getTimestamp())
                .code(code)
                .success(false)
                .message(message)
                .build());
  }

  private static long getTimestamp() {
    return System.currentTimeMillis() / 1000;
  }
}
