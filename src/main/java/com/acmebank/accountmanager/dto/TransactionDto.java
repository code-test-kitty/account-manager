package com.acmebank.accountmanager.dto;

import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TransactionDto {

  private String transactionId;

  @NotBlank private String formAccountName;

  @NotBlank private String toAccountName;

  @DecimalMin(value = "0.0", inclusive = false)
  private BigDecimal amount;
}
