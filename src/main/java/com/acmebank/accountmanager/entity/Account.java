package com.acmebank.accountmanager.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "ACCOUNT")
public class Account extends Auditable {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Integer id;

  private String accountNumber;

  private BigDecimal amount;

  private Integer decimalPlace;
}
