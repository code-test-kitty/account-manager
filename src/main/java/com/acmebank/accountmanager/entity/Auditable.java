package com.acmebank.accountmanager.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable {
  @CreatedBy
  @Column(name = "CREATED_BY", nullable = false, updatable = false)
  protected String createdBy;

  @CreatedDate
  @Column(name = "CREATED_DATE", nullable = false, updatable = false)
  protected LocalDateTime createdDate;

  @LastModifiedBy
  @Column(name = "UPDATED_BY", nullable = false)
  protected String updatedBy;

  @LastModifiedDate
  @Column(name = "UPDATED_DATE", nullable = false)
  protected LocalDateTime updatedDate;
}
