package com.acmebank.accountmanager.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "ACCOUNT_TRANSACTION")
public class Transaction extends Auditable {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Integer id;

  private Integer accountId;

  private String transactionId;

  private BigDecimal amount;

  private Integer decimalPlace;

  private boolean credit;
}
