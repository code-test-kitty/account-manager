package com.acmebank.accountmanager.config;

import com.acmebank.accountmanager.dto.RestResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestControllerAdvice {

  @ExceptionHandler({RuntimeException.class})
  ResponseEntity exception(RuntimeException e) {
    return RestResponseEntity.error(
        HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler({MethodArgumentNotValidException.class})
  ResponseEntity exception(MethodArgumentNotValidException e) {
    return RestResponseEntity.error(
        HttpStatus.BAD_REQUEST.value(), e.getMessage(), HttpStatus.BAD_REQUEST);
  }
}
