package com.acmebank.accountmanager.respository;

import com.acmebank.accountmanager.entity.Account;
import java.math.BigDecimal;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

  Optional<Account> findByAccountNumber(String accountNumber);

  Optional<Account> findByAccountNumberAndAmountGreaterThanEqual(
      String accountNumber, BigDecimal amount);
}
